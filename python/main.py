import json
import socket
import sys


class TheLBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.throttle = 0.9

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        print("I have joined")
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def submit(self):
        self.msg("throttle", self.throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_your_car(self, data):
        print("I am {0} and called {1}".format(data["color"], data["name"]))
        self.color = data["color"]
        self.ping()

    def on_game_init(self, data):
        print("Race has initialised")
        self.track = data["race"]["track"]
        self.ping()

    def on_game_start(self, data):
        print("Race has started")
        self.ping()

    def on_car_positions(self, data):
        for car in data:
            if (car["id"]["name"] == self.name and
                car["id"]["color"] == self.color):
                me = car
                break

        if abs(me["angle"]) > 30:
            self.throttle *= 0.05
        elif abs(me["angle"]) > 0:
            self.throttle *= 5
        else:
            self.throttle = 0.9

        self.throttle = min(0.9, self.throttle)
        print(self.throttle, me["angle"])
        self.submit()

    def on_crash(self, data):
        print("{0} has crashed".format(data["name"]))
        self.ping()

    def on_spawn(self, data):
        print("{0} has respawned".format(data["name"]))
        self.ping()

    def on_lap_finished(self, data):
        print("{0}, {1} has finished lap"
              "{2} in {3}".format(data["car"]["name"], data["car"]["color"],
                                  data["lapTime"]["lap"],
                                  data["lapTime"]["millis"]))

    def on_game_end(self, data):
        print("Race has ended")
        self.ping()

    def on_tournament_end(self, data):
        print("Tournament has ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            "yourCar": self.on_your_car,
            "gameInit": self.on_game_init,
            "gameStart": self.on_game_start,
            "carPositions": self.on_car_positions,
            "crash": self.on_crash,
            "spawn": self.on_spawn,
            "lapFinished": self.on_lap_finished,
            "gameEnd": self.on_game_end,
            "tournamentEnd": self.on_tournament_end,
            "error": self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg["msgType"], msg["data"]
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        raise Exception("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, "
              "key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = TheLBot(s, name, key)
        bot.run()
